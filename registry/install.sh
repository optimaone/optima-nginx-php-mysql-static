#! /bin/sh

docker run -d \
  -p 5050:5000 \
  --restart=always \
  --name registry \
  -v /mnt/registry:/var/lib/registry \
  registry:2